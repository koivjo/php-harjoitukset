<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 8</h2>
		<p class="tehtavananto">Tee HTML-lomake, jonka avulla käyttäjä voi antaa arvoja 
		(esim. auto, vene, lentokone). Kun käyttäjä painaa Lähetä-painiketta, tiedot lähetetään 
		erilliselle php-tiedostolle, joka tekee annetuista arvoista taulukon (array), lajittelee 
		taulukon aakkosten mukaan ja lopuksi tulostaa sen. Ks.
		http://ao.hamk.fi/saksa/php/harjoitukset/harj8.htm</p>
		
		<div class="tehtava">
			<form method="post" action="harj8tulosta.php">
				<p>Alkio 1
					<input type="text" name="alkio1">
				</p>
				<p>Alkio 2
					<input type="text" name="alkio2">
				</p>
				<p>Alkio 3
					<input type="text" name="alkio3">
				</p>
				<input type="submit" name="submit" value="L&auml;het&auml;">
			</form>
			<?php

			?>
		</div>
	</div> <!-- container -->
</body>
</html>