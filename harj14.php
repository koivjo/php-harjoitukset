<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 14</h2>
		<p class="tehtavananto">Jatka alla olevaa koodipätkää seuraavasti: Tee luvut-muuttujasta taulukko (array) ja etsi taulukosta isoin alkio ja tulosta se.
		<br>?php   $luvut = "123,432,543,23,523,43,1,5,432";
		</p>
		<div class="tehtava">
			<form method="post" action="harj14.php">
				<input type="submit" name="submit" value="Hae suurin alkio">
			</form>
			<?php
			/*
			$pisteet = "Tommi::3::2::4";
			$pisteettaulukko = explode("::",$pisteet); //muutetaan merkkijono taulukoksi
                                            //erotinmerkki on ::
			*/
			$luvut = "123,432,543,23,523,43,1,5,432";
			
			$luvutTaulukko = explode(",", $luvut);
			
			echo max($luvutTaulukko);
			
			?>
		</div>
	</div> <!-- container -->
</body>
</html>