<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 5</h2>
		<p class="tehtavananto">Php-tiedosto arpoo numeron (1-10), funktio: rand (minimi, maksimi). 
		K&auml;ytt&auml;j&auml; antaa numeron, mink&auml; j&auml;lkeen ohjelma antaa ilmoituksen onko k&auml;ytt&auml;j&auml;n antama numero
		ohjelman arpomaa numeroa pienempi, suurempi vai sama. Jos se on sama, ohjelma onnittelee 
		k&auml;ytt&auml;j&auml;&auml; hyv&auml;st&auml; suorituksesta.</p>
		
		<div class="tehtava">
			<form method="post" action="harj5.php">
				<input type="text" name="luku">
				<input type="submit" name="submit" value="L&auml;het&auml;">
			</form>
			<?php
				$luku = $_POST["luku"];
				$arpa = rand(1,10);
				echo "<p>Antamasi luku: " . $luku . "</p>" . "<p>Arvottu luku: " . $arpa . "</p>";
				if ($luku == $arpa){
					echo "Onneksi olkoon!";
				} else if($luku > $arpa) {
					echo "<p>Antamasi luku on suurempi.</p>";
				} else {
					echo "<p>Antamasi luku on pienempi.</p>";
				}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>