<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 11</h2>
		<p class="tehtavananto">Tee funktio, joka tulostaa parametrina saamansa tekstin punaisella. Teksti annetaan HTML-lomakkeella.</p>
		
		<div class="tehtava">
			<form method="post" action="harj11.php">
				<input type="text" name="tuloste" value="">
				<input type="submit" name="submit" value="Tulosta">
			</form>
			<?php
			$tuloste = $_POST["tuloste"];
			tulostaPunainen($tuloste);
			
			function tulostaPunainen($teksti){
				echo "<p style='color: red;'>" . $teksti ."</p>";
			}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>