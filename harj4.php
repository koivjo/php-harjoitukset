<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<p class="tehtavananto">Tee HTML-lomake, jossa kysyt&auml;&auml;n k&auml;ytt&auml;j&auml;n etu- ja sukunime&auml; 
		(kaksi tekstikentt&auml;&auml;). L&auml;het&auml;-painikkeen painamisen j&auml;lkeen k&auml;ytt&auml;j&auml;lle tulostuu Jatka-linkki, 
		jonka parametreina on etu- ja sukunimi (esim. harj4tulosta.php?etunimi=Matti&sukunimi=Lappi ). 
		Kun k&auml;ytt&auml;j&auml; painaa Jatka-linkki&auml;, tulostuu k&auml;ytt&auml;j&auml;n nimi. Tarvitset siis kolme tiedosta: 
		harj4lomake.htm, harj4jatka.php, harj4tulosta.php</p>
		
		<div class="tehtava">
			<form method="post" action="index.php">
				<p>Anna etunimesi:
					<input type="text" name="etunimi">
				</p>
				<p>Anna sukunimesi:
					<input type="text" name="sukunimi">
				</p>
				<input type="submit" name="submit" value="L&auml;het&auml;">
			</form>
			<?php
				$etunimi = $_POST["etunimi"];
				$sukunimi = $_POST["sukunimi"];
				echo "<a href='harj4tulosta.php?etunimi=" . $etunimi . "&sukunimi=" . $sukunimi . "'>Hei</a>";
			?>
		</div>
	</div> <!-- container -->
</body>
</html>