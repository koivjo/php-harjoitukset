<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 8 / Tulosta</h2>
		<p class="tehtavananto">Tee HTML-lomake, jonka avulla käyttäjä voi antaa arvoja 
		(esim. auto, vene, lentokone). Kun käyttäjä painaa Lähetä-painiketta, tiedot lähetetään 
		erilliselle php-tiedostolle, joka tekee annetuista arvoista taulukon (array), lajittelee 
		taulukon aakkosten mukaan ja lopuksi tulostaa sen. Ks.
		http://ao.hamk.fi/saksa/php/harjoitukset/harj8.htm</p>
		
		<div class="tehtava">
			<?php
				/*$alkio1 = $_POST["alkio1"];
				$alkio2 = $_POST["alkio2"];
				$alkio3 = $_POST["alkio3"];*/
				$alkioTaulukko = array($_POST["alkio1"], $_POST["alkio2"], $_POST["alkio3"]);
				sort ($alkioTaulukko);
				for ($i = 0; $i < 3; $i++){
					echo "<p>Alkio " . $i . " arvo on: " . $alkioTaulukko[$i] . "</p>";
				}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>