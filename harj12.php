<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 12</h2>
		<p class="tehtavananto">Tee ohjelma, joka muodostaa ja tulostaa käyttäjätunnuksen. Käyttäjä antaa HTML-lomakkeella etu- ja sukunimensä. Ohjelma tekee tunnuksen seuraavasti: 
		<br>• muuttaa kirjaimet pieniksi kirjaimiksi
		<br>• ä>a, ö>o, å>a (Huom! Jätä tämä kohta tekemättä, jos teet ao-palvelimella)
		<br>• käyttäjätunnus: vi + sukunimen 5 ensimmäistä kirjainta + etunimen 2 ensimmäistä kirjainta (esim. Tommi Saksa > visaksato)</p>
		
		<div class="tehtava">
			<form method="post" action="harj12.php">
				<p>Etunimi:
				<input type="text" name="etunimi" value="<?php echo $_POST["etunimi"];?>">
				</p>
				<p>Sukunimi:
				<input type="text" name="sukunimi" value="<?php echo $_POST["sukunimi"];?>">
				</p>
				<input type="submit" name="submit" value="Luo käyttäjätunnus">
			</form>
			<?php
			$etunimi = $_POST["etunimi"];
			$sukunimi = $_POST["sukunimi"];
			luoKayttajatunnus($etunimi, $sukunimi);
			
			function luoKayttajatunnus($etunimi, $sukunimi){
				/*strtolower($etunimi);
				strtolower($sukunimi);*/
				$etunimi = strtr($etunimi, "äåö", "aao");
				$sukunimi = strtr($sukunimi, "äåö", "aao");
				echo "<p>Käyttäjätunnuksesi on: " . "vi" . strtolower(substr($sukunimi,0,5) . substr($etunimi,0,2)) . "</p>";
			}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>