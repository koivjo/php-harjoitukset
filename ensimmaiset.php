<!DOCTYPE html>
<html>
<head>

	<title>PHP-harjoitukset</title>
	<link rel="stylesheet" href="main.css">
	
</head>
<body>

	<header><h1>PHP-harjoitukset</h1></header>

	<div class="container">	
		
		<label>Joni Koivula</label>
		<label>TRTKNU15</label>
		
		<article>
			<h2>Harjoitus 1</h2>
			
			<p class="tehtavananto"><b>1.1</b> Tee HTML-lomake, johon k&auml;ytt&auml;j&auml; kirjoittaa kaksi numeroa. Kun k&auml;ytt&auml;j&auml; painaa 
			L&auml;het&auml;-painiketta, PHP-tiedosto laskee numeroiden summan ja tulostaa sen k&auml;ytt&auml;j&auml;lle.</p>
			
			<div class="tehtava">
			
				<form method="post" action="index.php">

					<input type="text" name="luku1">
					<input type="text" name="luku2">
					<input type="submit" name="Submit" value="Laske summa">
		
				</form>
				
				<?php
					$luku1 = $_POST["luku1"];
					$luku2 = $_POST["luku2"];
					$summa = $luku1 + $luku2;
					echo "<p>Summa on: " . $summa . "</p>";
				?>

			</div>
			
			<p class="tehtavananto"><b>1.2</b> Tee HTML-lomake, johon k&auml;ytt&auml;j&auml; antaa jonkin numeron. Kun k&auml;ytt&auml;j&auml; painaa L&auml;het&auml;-painiketta,
			k&auml;ytt&auml;j&auml;lle tulostuu numerot k&auml;ytt&auml;j&auml;n antamasta numerosta nollaan. K&auml;yt&auml; tulostamiseen for:ia.</p>
			
			<div class="tehtava">
				<form method="post" action="index.php">
					<input type="text" name="alkuluku">
					<input type="submit" name="Submit" value="L&auml;het&auml;">
				</form>
			
				<?php
					$alkuluku = $_POST["alkuluku"];
					for ($i = $alkuluku; $i>=0; $i--){
						echo $i . " ";
					}
				?>

			</div>
			
			<p class="tehtavananto"><b>1.3</b> Tee HTML-lomake, jossa kysyt&auml;&auml;n k&auml;ytt&auml;j&auml;n nime&auml;. Kun k&auml;ytt&auml;j&auml; painaa L&auml;het&auml;-painiketta, 
			tarkistetaan onko tekstikentt&auml; tyhj&auml;. Jos kentt&auml; on tyhj&auml;, huomautetaan asiasta k&auml;ytt&auml;j&auml;&auml;, muuten tulostetaan 
			'Nimesi on...'</p>
			
			<div class="tehtava">
			
			<form method="post" action="index.php">
				<p>Anna etunimesi
				<input type="text" name="etunimi">
				<input type="submit" name="Submit" value="L&auml;het&auml;">
			</form>
			
			<?php
				$etunimi = $_POST["etunimi"];
				if ($etunimi == "") {
					echo "Et antanut nimeasi.";
				} else {
					echo "Jee!";
				}
			?>

			</div>
			
			<p class="tehtavananto"><b>1.4</b> Tee HTML-lomake, jossa kysyt&auml;&auml;n k&auml;ytt&auml;j&auml;n etu- ja sukunime&auml; (kaksi tekstikentt&auml;&auml;). 
			L&auml;het&auml;-painikkeen painamisen j&auml;lkeen k&auml;ytt&auml;j&auml;lle tulostuu Jatka-linkki, jonka parametreina on etu- ja sukunimi 
			(esim. harj4tulosta.php?etunimi=Matti&sukunimi=Lappi ). Kun k&auml;ytt&auml;j&auml; painaa Jatka-linkki&auml;, tulostuu k&auml;ytt&auml;j&auml;n nimi. 
			Tarvitset siis kolme tiedosta: harj4lomake.htm, harj4jatka.php, harj4tulosta.php </p>
			
			<div class="tehtava">

			</div>
			
			<p class="tehtavananto"><b>1.5</b> Php-tiedosto arpoo numeron (1-10), funktio: rand (minimi, maksimi). K&auml;ytt&auml;j&auml; antaa numeron, 
			mink&auml; j&auml;lkeen ohjelma antaa ilmoituksen onko k&auml;ytt&auml;j&auml;n antama numero ohjelman arpomaa numeroa pienempi, suurempi vai 
			sama. Jos se on sama, ohjelma onnittelee k&auml;ytt&auml;j&auml;&auml; hyv&auml;st&auml; suorituksesta.</p>
			
			<div class="tehtava">

			</div>
			
		</article>
</body>
</html>