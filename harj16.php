<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 16</h2>
		<p class="tehtavananto">Tee yksinkertainen kirjautumissysteemi (katso esimerkki). Ensimmäisellä sivulla on lomake, johon kirjoitetaan salasana (esimerkissä qwerty). Kun salasana on kirjoitettu, siirrytään toiselle sivulle, jossa tutkitaan, onko salasana annettu oikein. Jos salasana on oikein, lisätään sessio-muuttujaan tieto, että salasana on ok tai että käyttäjä on kirjautunut onnistuneesti. Salasanan tarkitussivulla (esimerkissä http://ao.hamk.fi/saksa/php/harjoitukset/sessio/yllapito.php) tutkitaan myös sessio-muuttujaa käyttäen, onko kirjauduttu ok. Jos on, tulostetaan "olet kirjautunut", muussa tapauksessa "et ole kirjautunut". Kun tieto onnistuneesta kirjautumisesta on tallennettu sessio-muuttujaan, voit onnistuneen kirjautumisen jälkeen kokeilla siirtyä selaimella toisille sivuille ja palata takaisin kirjautumisen tarkistussivulle. Sivun pitäisi nyt "muistaa", että olet kirjautunut ok (ilman siis menemällä varsinaisen lomakesivun kautta). 
		</p>
		<p class="tehtavananto">Jos haluat entisestään kehittää systeemiä, tee kirjautumissivulle myös tarkistus, jossa sessio-muuttujaa käyttäen tarkistetaan onko jo kirjauduttu ok. Jos kirjautuminen on ok, siirretään käyttäjä suoraan kirjautumisen tarkistussivulle (joka siis tulostaa, että "olet kirjautunut"). PHP:llä toiselle sivulle siirrytään komennolla: header("Location: http://www.hamk.fi"); Kirjautumisen tarkistussivulle voisi tehdä "kirjaudu ulos" -linkin, joka siis poistaa sessio-muuttujasta onnistuneen kirjautumistiedon (tai koko sessio-muuttujan) ja palauttaa käyttäjän kirjautumissivulle. 
		</p>
		<div class="tehtava">
			<?php

			?>
		</div>
	</div> <!-- container -->
</body>
</html>