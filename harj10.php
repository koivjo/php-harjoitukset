<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 10</h2>
		<p class="tehtavananto">Tee funktio, joka laskee huoneen tilavuuden 
		(pituus * leveys * korkeus). Pituus, leveys ja korkeus annetaan funktiolle parametreina. 
		Huom! Laskutoimitus siis pitää kirjoittaa funktioon!!</p>
		
		<div class="tehtava">
			<form method="post" action="harj10.php">
				<p>Pituus:
					<input type="text" name="pituus" value="<?php echo $_POST["pituus"]; ?>">
				</p>
				<p>Leveys:
					<input type="text" name="leveys" value="<?php echo $_POST["leveys"]; ?>">
				</p>
				<p>Korkeus:
					<input type="text" name="korkeus" value="<?php echo $_POST["korkeus"]; ?>">
				</p>
				<input type="submit" name="submit" value="L&auml;het&auml;">
			</form>

			<?php
			$pituus = $_POST["pituus"];
			$leveys = $_POST["leveys"];
			$korkeus = $_POST["korkeus"];
			laskeTilavuus($pituus, $leveys, $korkeus);
			
			function laskeTilavuus($pituus, $leveys, $korkeus){
				$tilavuus = $pituus * $leveys * $korkeus;
				echo "<p>Tilavuus on: " . $tilavuus . "m3</p>";
			}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>