<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 6 ja 7</h2>
		<p class="tehtavananto">Harjoitus 6: Tee ohjelma, joka luo taulukon (array) 
		ja tulostaa tiedot HTML-taulukkona.</p>
		
		<div class="tehtava">

			<?php
				$taulukko1 = array("Jenni", "Moona", "Pasi");
				
				echo "<table>";
				foreach ($taulukko1 as $data){
					echo "<tr><td>" . $data . "</td></tr>";
				}
				echo "</table>";
			?>
		</div>
		
		<p class="tehtavananto">Harjoitus 7</p>
		
		<div class="tehtava">

			<?php
				$taulukko2 = array("a","c","b","e","d","g","f");
				$alkioita = count($taulukko2);
				echo "<table>";
				echo "<tr>";
				for ($i = 0; $i < $alkioita; $i++){
					echo "<td>" . $i . "</td>";
				}
				echo "</tr>";
				echo "<tr>";
				foreach ($taulukko2 as $data){
					echo "<td>" . $data . "</td>";
				}
				echo "</tr>";
				echo "</table>";
			?>
			<a href="harj6.php"><?php rsort($taulukko2); ?>Kaanteisessa jarjestyksessa</a>
		</div>
		
	</div> <!-- container -->
</body>
</html>