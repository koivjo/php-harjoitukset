<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 13</h2>
		<p class="tehtavananto">Käyttäjä antaa kolmen värin RGB-arvot (0-255) HTML-lomakkeella. Ohjelma tulostaa värin heksadesimaalimuodon, esim. Red = 0, Green = 102, Blue = 255 > 066FF. Käytä apunasi funktiota: dechex().
		</p>
		<div class="tehtava">
			<form method="post" action="harj13.php">
				<p>Red:
				<input type="text" name="red" value="<?php echo $_POST["red"];?>">
				</p>
				<p>Green:
				<input type="text" name="green" value="<?php echo $_POST["green"];?>">
				</p>
				<p>Blue:
				<input type="text" name="blue" value="<?php echo $_POST["blue"];?>">
				</p>
				<input type="submit" name="submit" value="Lähetä">
			</form>
			<?php
			$red = $_POST["red"];
			$green = $_POST["green"];
			$blue = $_POST["blue"];
			$vari = dechex($red) . dechex($green) . dechex($blue);
			//echo "<br>Hexadecimaali väri on: " . $vari . "<br><br>";
			echo "<p style='padding: 50px 0; color: white;
				background-color: #" . $vari . "'>Hexadecimaali väri on: " . $vari . "</p>";
			?>
		</div>
	</div> <!-- container -->
</body>
</html>