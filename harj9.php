<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 9</h2>
		<p class="tehtavananto">Käyttäjä antaa numeron väliltä 1 - 9 HTML-lomakkeella. 
		Tämän jälkeen tutkitaan, löytyykö ko. numero taulukosta, joka on muodostettu seuraavasti:<br>
		$numerot = array(5,3,8,7,1,9);  <br>
		Jos löytyy, tulostetaan "Numero löytyi!". Haastetta saat, jos tulostat "Ei löytynyt", 
		jos numeroa ei löytynyt. Vinkki: break-komento lopettaa for-lauseen looppauksen.</p>
		
		<div class="tehtava">
			<form method="post" action="harj9.php">
				<p>Löytyykö luku:
					<input type="text" name="luku">
				</p>
				<input type="submit" name="submit" value="Kysy">
			</form>
			<?php
			$numerot = array(5,3,8,7,1,9);
			$numerotPituus = count($numerot);
			$luku = $_POST["luku"];
			for ($i = 0; $i <= $numerotPituus; $i++){
				if ($luku == $numerot[$i]){
					echo "Luku löytyi!";
					break;
				} else if($i == $numerotPituus){
					echo "Lukua ei löytynyt :(";
				}
			}
			?>
		</div>
	</div> <!-- container -->
</body>
</html>