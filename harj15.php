<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="main.css">
</head>
<body>
	<h1>PHP-harjoituksia</h1>
	<div class="container">
		<h2>Harjoitus 15</h2>
		<p class="tehtavananto">Tulosta aika seuraavasti: <br>
		<i>Date: Wednesday, 23 October 2002<br>
		Time: 22:13</i>
		</p>
		<div class="tehtava">
			<?php
			/*
			echo "muotoilematon aika: ".time();
			$aika = getdate(time());
			echo "<br>muotoiltu aika: ".$aika['hours'].":".$aika['minutes'];
			?>
			*/
			$aika = getdate(time());
			echo "<p><i>Date: ".$aika['weekday'].", ".$aika['mday']." ".$aika['month']." ".$aika['year']."</i></p>";
			echo "<p><i>Time: ".$aika['hours'].":".$aika['minutes']."</i></p>";
			?>
		</div>
	</div> <!-- container -->
</body>
</html>